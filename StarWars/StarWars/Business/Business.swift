//
//  Business.swift
//  StarWars
//
//  Created by DXBSS-MACLTP on 10/4/19.
//  Copyright © 2019 DXBSS-MACLTP. All rights reserved.
//

import UIKit
import SwiftyJSON

class Business: NSObject {
    class func getAllFilms(json : JSON) -> [Film] {
        
        var arrayFilms = [Film]()
        
        let results = json["results"].array ?? []
        for item in results {
            arrayFilms.append(Film(jsonObject: item))
        }
        return arrayFilms
    }
    
    class func getLongestOpeningCrawl(filmsArray : [Film]) -> String {
        
        var longestOpenCrawelFilm : Film! = nil
        
        if(filmsArray.count > 0) {
            for film in filmsArray {
                if(longestOpenCrawelFilm == nil) {
                    longestOpenCrawelFilm = film
                }
                else {
                    if(film.opening_crawl.count > longestOpenCrawelFilm.opening_crawl.count) {
                        longestOpenCrawelFilm = film
                    }
                }
            }
        }
        
        if(longestOpenCrawelFilm == nil) {
            return ""
        }
        else {
            return longestOpenCrawelFilm.title
        }
    }
    
    
    class func getMostAppearCharchter(filmsArray : [Film]) -> String {
        
        var charachterUrl = ""
        var allCharachters = [String]()
        
        if(filmsArray.count > 0) {
            filmsArray.forEach { film in allCharachters.append(contentsOf: film.characters) }
        }
        
        let countedSet = NSCountedSet(array: allCharachters)
        charachterUrl = countedSet.max { countedSet.count(for: $0) < countedSet.count(for: $1) } as! String
        return charachterUrl
    }
    
    
    class func getMostAppearSpecies(filmsArray : [Film]) -> (listOfSpeciesUrls : [String] , numberOfrepeate : Int) {
        
        var maxNumberOfRepeated = 0
        
        var allSpecies = [String]()
        filmsArray.forEach { film in allSpecies.append(contentsOf: film.species) }
        
        
        var counts = [String: Int]()
        allSpecies.forEach { counts[$0] = (counts[$0] ?? 0) + 1 }
        
        // Find the most frequent value and its count with max(by:)
        if let (result) = counts.max(by: {$0.1 < $1.1}) {
            maxNumberOfRepeated = result.value
        }
        
        
        let maxItemsDuplicated = Array(Set(allSpecies.filter({ (i: String) in allSpecies.filter({ $0 == i }).count > maxNumberOfRepeated - 1})))
        
        return (maxItemsDuplicated , maxNumberOfRepeated)
    }
}
