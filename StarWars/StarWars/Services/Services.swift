//
//  Company.swift
//  DPW
//
//  Created by DXBSS-MACLTP on 6/20/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Services: NSObject {
    
    static let domain = "https://swapi.co/api/"
    
    
    class func getAllFilms(completion: @escaping (_ error: Error?, _ result: [Film])->Void) {
        let url = domain + "films/"
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                switch response.result {
                case .failure(let error):
                    completion(error, [])
                    print(error)
                    
                case .success(let value):
                    let jsonResult = JSON(value)
                    completion(nil, Business.getAllFilms(json: jsonResult))
                }
        }
    }
    
    class func getObject(url : String , completion: @escaping (_ error: Error?, _ object: JSON?)->Void) {
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                switch response.result {
                case .failure(let error):
                    completion(error, nil)
                    print(error)
                    
                case .success(let value):
                    let jsonResult = JSON(value)
                    completion(nil, jsonResult)
                }
        }
    }
}
