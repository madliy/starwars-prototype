//
//  Character.swift
//  StarWars
//
//  Created by DXBSS-MACLTP on 10/4/19.
//  Copyright © 2019 DXBSS-MACLTP. All rights reserved.
//

import UIKit
import SwiftyJSON

class Character: NSObject {
    var name = ""
    
    init(jsonObject : JSON) {
        name = jsonObject["name"].stringValue
    }
    
}
