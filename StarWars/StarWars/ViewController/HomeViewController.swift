//
//  HomeViewController.swift
//  StarWars
//
//  Created by DXBSS-MACLTP on 10/4/19.
//  Copyright © 2019 DXBSS-MACLTP. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableButton: UIButton {
}

class HomeViewController: UIViewController {
    
    @IBOutlet weak var loading: UIActivityIndicatorView!

    @IBOutlet weak var answer2loading: UIActivityIndicatorView!
    @IBOutlet weak var answer3loading: UIActivityIndicatorView!
    @IBOutlet weak var answer4loading: UIActivityIndicatorView!

    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var leftStarButton: UIButton!
    @IBOutlet weak var rightStartButton: UIButton!

    @IBOutlet weak var answer1Label: UILabel!
    @IBOutlet weak var answer2Label: UILabel!
    @IBOutlet weak var answer3Label: UILabel!
    @IBOutlet weak var answer4Label: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    
    @IBAction func getQuestionAnswersAction(_ sender: Any) {
        if(actionButton.isSelected) {
            loading.startAnimating()
            Services.getAllFilms { (error, filmsArray) in
                self.loading.stopAnimating()
                self.answerQuestion1(filmsArray: filmsArray)
                self.answerQuestion2(filmsArray: filmsArray)
                self.answerQuestion3(filmsArray: filmsArray)
                self.answer4Label.text = "--"
            }
        }
        else {
            resetValues()
        }
    }

    
    @IBAction func heighlightAction(_ sender: Any) {
        self.selectButtons(isSelected: !actionButton.isSelected)
    }
    
    func selectButtons(isSelected : Bool) {
        leftStarButton.isSelected = isSelected
        rightStartButton.isSelected = isSelected
        actionButton.isSelected = isSelected
    }
    
    func answerQuestion1 (filmsArray : [Film]) {
        let answer1 = Business.getLongestOpeningCrawl(filmsArray: filmsArray)
        answer1Label.text = answer1
    }
    
    
    func answerQuestion2 (filmsArray : [Film]) {
        let charachterUrl = Business.getMostAppearCharchter(filmsArray: filmsArray)
        answer2loading.startAnimating()
        Services.getObject(url: charachterUrl) { (error, object) in
            self.answer2loading.stopAnimating()
            let character = Character(jsonObject: object!)
            self.answer2Label.text = character.name
        }
        
    }
    
    
    
    
    func answerQuestion3 (filmsArray : [Film]) {
        answer3loading.startAnimating()
        let result = Business.getMostAppearSpecies(filmsArray: filmsArray)
        let listOfSpeciesUrls = result.listOfSpeciesUrls
        let numberOfrepeate = result.numberOfrepeate
        var resultString = ""
        for url in listOfSpeciesUrls {
            Services.getObject(url: url) { (error, object) in
                self.answer2loading.stopAnimating()
                let specie = Specie(jsonObject: object!)
                
                if(resultString == "") {
                    resultString = "\(specie.name) (\(numberOfrepeate))"
                }
                else {
                    resultString = "\(resultString) \n \(specie.name) (\(numberOfrepeate))"
                }
                self.answer3Label.text = resultString
                if(url == listOfSpeciesUrls.last) {
                    self.answer3loading.stopAnimating()
                }
            }
        }
    }
    
    func resetValues() {
        answer1Label.text = ""
        answer2Label.text = ""
        answer3Label.text = ""
        answer4Label.text = ""
    }
    
}
