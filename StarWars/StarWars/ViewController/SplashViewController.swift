//
//  SplashViewController.swift
//  StarWars
//
//  Created by DXBSS-MACLTP on 10/4/19.
//  Copyright © 2019 DXBSS-MACLTP. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    @IBOutlet weak var heightConstraints: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let newConstraint = NSLayoutConstraint(
            item: heightConstraints.firstItem as Any,
            attribute: heightConstraints.firstAttribute,
            relatedBy: heightConstraints.relation,
            toItem: heightConstraints.secondItem,
            attribute: heightConstraints.secondAttribute,
            multiplier: 0.25,
            constant: heightConstraints.constant)
        
        DispatchQueue.main.async {
            NSLayoutConstraint.deactivate([self.heightConstraints])
            NSLayoutConstraint.activate([newConstraint])
            UIView.animate(withDuration: 3.0) {
                self.view.layoutIfNeeded()
            }
        }
        Timer.scheduledTimer(withTimeInterval: 0.0, repeats: false) { (Timer) in
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "home", sender: nil)
            }
        }
        
        
    }
    
}
